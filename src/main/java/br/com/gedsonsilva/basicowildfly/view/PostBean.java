package br.com.gedsonsilva.basicowildfly.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.gedsonsilva.basicowildfly.model.Post;
import br.com.gedsonsilva.basicowildfly.services.interfaces.IPostService;

@Named
@ViewScoped
public class PostBean implements Serializable{

	private static final long serialVersionUID = -4693521728029672904L;

	String description = "Simple description";
	String post = "O posto mais epico do mundo";
	
	@Inject
	IPostService postService;

	public PostBean() {
	}

	@PostConstruct
	public void init(){
		postService.save(new Post("Salvo pelo bean aos " + System.currentTimeMillis(), "Descri��o sinistra aos "+ System.currentTimeMillis()));
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}
	

	
	
}
