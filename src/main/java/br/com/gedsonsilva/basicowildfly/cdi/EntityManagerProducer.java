package br.com.gedsonsilva.basicowildfly.cdi;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.web.context.annotation.ApplicationScope;

@ApplicationScope
public class EntityManagerProducer {
	@PersistenceContext(name = "basicojsf")
	private EntityManager em;

	@Produces
	@RequestScoped
	public EntityManager getEm() {
		return em;
	}

}