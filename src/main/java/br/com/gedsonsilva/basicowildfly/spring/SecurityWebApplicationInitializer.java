package br.com.gedsonsilva.basicowildfly.spring;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityWebApplicationInitializer
extends AbstractSecurityWebApplicationInitializer {

public SecurityWebApplicationInitializer() {
	super(WebSecurityConfig.class);
}
}