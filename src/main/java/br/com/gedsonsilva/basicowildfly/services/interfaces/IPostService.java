package br.com.gedsonsilva.basicowildfly.services.interfaces;

import java.io.Serializable;

import br.com.gedsonsilva.basicowildfly.model.Post;

public interface IPostService extends Serializable{

	public Post save(Post post);
}
