package br.com.gedsonsilva.basicowildfly.services;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import br.com.gedsonsilva.basicowildfly.model.Post;
import br.com.gedsonsilva.basicowildfly.services.interfaces.IPostService;

@RequestScoped
public class PostServiceImpl implements IPostService {

	private static final long serialVersionUID = -2787489712955235650L;

	@Inject
	EntityManager entityManager;
	
	@Override
	@Transactional
	public Post save(Post post) {
		return entityManager.merge(post);
	}

}
